const selectSort = require('./selectSort');
const insertSort = require('./insertSort');
const bubbleSort = require('./bubbleSort');


function shuffle(arr) {
    for (let i = 0; i < arr.length; i++) {
        let random = Math.floor(Math.random() * arr.length);
        [arr[i], arr[random]] = [arr[random], arr[i]];
    }
    return arr;
}

const array = shuffle(new Array(50).fill(0).map((_, i) => i + 1));

console.log("Shuffle array: ", array);

function testSort(sort, array, sortType = 'none') {
    const defaultSortResult = array.sort((a, b) => a - b);
    const sortResult = sort(array);
    console.log(`Sort type: ${sortType} - ${defaultSortResult.toString() === sortResult.toString() ? 'OK' : 'FAIL'}`);
    console.log({
        defaultSortResult,
        sortResult
    });
    
}

testSort(selectSort, array, 'selectSort');
testSort(insertSort, array, 'insertSort');
testSort(bubbleSort, array, 'bubbleSort');
